import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignsItemComponent } from './campaigns-item.component';

describe('CampaignsItemComponent', () => {
  let component: CampaignsItemComponent;
  let fixture: ComponentFixture<CampaignsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
