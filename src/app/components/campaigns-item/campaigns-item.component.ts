import { CampaignService } from 'src/app/providers/campaign.service';
import {  Component,    OnInit } from '@angular/core';

@Component({
  selector: 'app-campaigns-item',
  templateUrl: './campaigns-item.component.html',
  styleUrls: ['./campaigns-item.component.css']
})
export class CampaignsItemComponent implements OnInit {

  data_campaign: any;
  constructor(private _campaign: CampaignService ) { }

  ngOnInit() {
    this._campaign.campaign_item.subscribe(resp => {      
      this.data_campaign = resp;
      this.data_campaign.actions.map(x => {
        x.checked = false;
      });
      this._campaign.campaign_checked.emit(this.data_campaign);
    });
  }

  checkStatus(evt , id: number) {
    console.log(evt.target.checked);
    const index = this.data_campaign.actions.findIndex(x => x._id === id);
    if(index == -1) {
      return;
    }
    this.data_campaign.actions[index].checked = evt.target.checked;
    this._campaign.campaign_checked.emit(this.data_campaign);
    console.log(this.data_campaign);  
  }

}
