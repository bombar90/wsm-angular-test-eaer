import { Component, OnInit } from '@angular/core';
import { CampaignService } from 'src/app/providers/campaign.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {

  cg_data: any;
  campaign_data: any;
  count_campagins: number =0;
  disable = true;
  constructor( private _campagin: CampaignService) { }

  ngOnInit() {

    this._campagin.getCampaignGroup().subscribe(resp => {
      this.cg_data = resp[0];
      this._campagin.getCampaign().subscribe( (resp: any[]) => {        
        console.log(resp);
        this.campaign_data = resp[0];
        this.count_campagins = resp.length;
      });
    });

    this._campagin.campaign_checked.subscribe(resp => {
      const check_actions = resp.actions.filter(x=> x.checked == true);
      if(check_actions.length > 0) {
        this.disable = false;
      } else {
        this.disable = true;
      }
    });
  }

}
