import { CampaignService } from './../../../providers/campaign.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  items_camp : any[] =[];
  selectedItem: any;
  constructor(private _campaign : CampaignService ) { }

  ngOnInit() {
    this._campaign.getCampaignItems().subscribe( (resp: any[]) => {
      console.log(resp);
      this.selectedItem = resp[0];      
      this.items_camp = resp;
      this._campaign.campaign_item.emit(this.selectedItem);
    });
  }

  listClick(event, newValue) {
    console.log(newValue);  
    this.selectedItem = newValue;
    this._campaign.campaign_item.emit(this.selectedItem);    
}



}
