import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { BreadcrumbComponent } from './components/shared/breadcrumb/breadcrumb.component';
import { HttpClientModule } from '@angular/common/http';
import { SidebarComponent } from './components/shared/sidebar/sidebar.component';
import { CampaignsItemComponent } from './components/campaigns-item/campaigns-item.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BreadcrumbComponent,
    SidebarComponent,
    CampaignsItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
