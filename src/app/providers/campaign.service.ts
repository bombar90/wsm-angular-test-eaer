import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {  
  campaign_item = new EventEmitter<any>();
  campaign_checked = new EventEmitter<any>();
  constructor( private http: HttpClient ) {}
  getCampaignGroup() {
    let url = 'https://7e1ec65d-6cc3-4a1e-a781-c265f6cc45da.mock.pstmn.io/campaign-groups/?id=1';    
    url = encodeURI(url);

    const headers = new HttpHeaders()      
      .set('Content-Type', 'application/json');
    return this.http.get(url, {headers: headers}).pipe( map( resp => {
      return resp;
    }));  
}
  getCampaign() {
    let url = 'https://7e1ec65d-6cc3-4a1e-a781-c265f6cc45da.mock.pstmn.io/campaigns/';    
    url = encodeURI(url);

    const headers = new HttpHeaders()      
      .set('Content-Type', 'application/json');
    return this.http.get(url, {headers: headers}).pipe( map( resp => {
      return resp;
    }));  
  }
  
  getCampaignItems () {
    let url = 'https://7e1ec65d-6cc3-4a1e-a781-c265f6cc45da.mock.pstmn.io/items/';    
    url = encodeURI(url);
  
    const headers = new HttpHeaders()      
      .set('Content-Type', 'application/json');
    return this.http.get(url, {headers: headers}).pipe( map( resp => {
      return resp;
    }));  
  }

}